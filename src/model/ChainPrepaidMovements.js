/**
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The ChainPrepaidMovements model module.
* @module model/ChainPrepaidMovements
* @version 2.0.0
*/
export default class ChainPrepaidMovements {
    /**
    * Constructs a new <code>ChainPrepaidMovements</code>.
    * @alias module:model/ChainPrepaidMovements
    * @class
    * @param customerUuid {String} 
    * @param validFrom {Date} 
    * @param credit {Number} 
    * @param ticketCredit {Number} 
    * @param movementTypeId {Number} 
    */

    constructor(customerUuid, validFrom, credit, ticketCredit, movementTypeId) {
        
        
        this['customer_uuid'] = customerUuid;
        this['valid_from'] = validFrom;
        this['credit'] = credit;
        this['ticket_credit'] = ticketCredit;
        this['movement_type_id'] = movementTypeId;
        
    }

    /**
    * Constructs a <code>ChainPrepaidMovements</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/ChainPrepaidMovements} obj Optional instance to populate.
    * @return {module:model/ChainPrepaidMovements} The populated <code>ChainPrepaidMovements</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ChainPrepaidMovements();
                        
            
            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('customer_uuid')) {
                obj['customer_uuid'] = ApiClient.convertToType(data['customer_uuid'], 'String');
            }
            if (data.hasOwnProperty('previous_id')) {
                obj['previous_id'] = ApiClient.convertToType(data['previous_id'], 'Number');
            }
            if (data.hasOwnProperty('shop_uuid')) {
                obj['shop_uuid'] = ApiClient.convertToType(data['shop_uuid'], 'String');
            }
            if (data.hasOwnProperty('valid_from')) {
                obj['valid_from'] = ApiClient.convertToType(data['valid_from'], 'Date');
            }
            if (data.hasOwnProperty('valid_to')) {
                obj['valid_to'] = ApiClient.convertToType(data['valid_to'], 'Date');
            }
            if (data.hasOwnProperty('credit')) {
                obj['credit'] = ApiClient.convertToType(data['credit'], 'Number');
            }
            if (data.hasOwnProperty('ticket_credit')) {
                obj['ticket_credit'] = ApiClient.convertToType(data['ticket_credit'], 'Number');
            }
            if (data.hasOwnProperty('movement_type_id')) {
                obj['movement_type_id'] = ApiClient.convertToType(data['movement_type_id'], 'Number');
            }
            if (data.hasOwnProperty('sale_uuid')) {
                obj['sale_uuid'] = ApiClient.convertToType(data['sale_uuid'], 'String');
            }
            if (data.hasOwnProperty('notes')) {
                obj['notes'] = ApiClient.convertToType(data['notes'], 'String');
            }
            if (data.hasOwnProperty('createdby_id')) {
                obj['createdby_id'] = ApiClient.convertToType(data['createdby_id'], 'Number');
            }
            if (data.hasOwnProperty('amount')) {
                obj['amount'] = ApiClient.convertToType(data['amount'], Object);
            }
            if (data.hasOwnProperty('ticket_amount')) {
                obj['ticket_amount'] = ApiClient.convertToType(data['ticket_amount'], Object);
            }
            if (data.hasOwnProperty('movement_type')) {
                obj['movement_type'] = ApiClient.convertToType(data['movement_type'], Object);
            }
        }
        return obj;
    }

    /**
    * @member {Number} id
    */
    'id' = undefined;
    /**
    * @member {String} customer_uuid
    */
    'customer_uuid' = undefined;
    /**
    * @member {Number} previous_id
    */
    'previous_id' = undefined;
    /**
    * @member {String} shop_uuid
    */
    'shop_uuid' = undefined;
    /**
    * @member {Date} valid_from
    */
    'valid_from' = undefined;
    /**
    * @member {Date} valid_to
    */
    'valid_to' = undefined;
    /**
    * @member {Number} credit
    */
    'credit' = undefined;
    /**
    * @member {Number} ticket_credit
    */
    'ticket_credit' = undefined;
    /**
    * @member {Number} movement_type_id
    */
    'movement_type_id' = undefined;
    /**
    * @member {String} sale_uuid
    */
    'sale_uuid' = undefined;
    /**
    * @member {String} notes
    */
    'notes' = undefined;
    /**
    * @member {Number} createdby_id
    */
    'createdby_id' = undefined;
    /**
    * @member {Object} amount
    */
    'amount' = undefined;
    /**
    * @member {Object} ticket_amount
    */
    'ticket_amount' = undefined;
    /**
    * @member {Object} movement_type
    */
    'movement_type' = undefined;




}
