/**
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from "../ApiClient";
import Rooms from '../model/Rooms';

/**
* Rooms service.
* @module api/RoomsApi
* @version 2.0.0
*/
export default class RoomsApi {

    /**
    * Constructs a new RoomsApi. 
    * @alias module:api/RoomsApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }

    /**
     * Callback function to receive the result of the roomsGet operation.
     * @callback module:api/RoomsApi~roomsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Rooms} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get All Rooms
     * Returns a Json with data about all rooms of selected shop.  Paginated by default (per_page&#x3D;1000)
     * @param {Object} opts Optional parameters
     * @param {module:api/RoomsApi~roomsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Rooms}
     */
    roomsGet(opts, callback) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'pagination': opts['pagination'],
        'per_page': opts['perPage'],
        'page': opts['page']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Rooms;

      return this.apiClient.callApi(
        '/rooms', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
    /**
     * Callback function to receive the result of the roomsIdDelete operation.
     * @callback module:api/RoomsApi~roomsIdDeleteCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Delete existing Room
     * In this case you must specify the id in the URL, but it is no necessary in the request body
     * @param {module:api/RoomsApi~roomsIdDeleteCallback} callback The callback function, accepting three arguments: error, data, response
     */
    roomsIdDelete(id, callback) {
      let postBody = null;

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = [];
      let accepts = [];
      let returnType = null;

      return this.apiClient.callApi(
        '/rooms/{id}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
    /**
     * Callback function to receive the result of the roomsIdGet operation.
     * @callback module:api/RoomsApi~roomsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Rooms} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get existing Printer
     * In this case you must specify the id in the URL
     * @param {module:api/RoomsApi~roomsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Rooms}
     */
    roomsIdGet(id, callback) {
      let postBody = null;

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Rooms;

      return this.apiClient.callApi(
        '/rooms/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
    /**
     * Callback function to receive the result of the roomsIdPut operation.
     * @callback module:api/RoomsApi~roomsIdPutCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Rooms} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Edit existing Room
     * In this case you must specify the id in the URL and change the data you wanna update
     * @param {module:api/RoomsApi~roomsIdPutCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Rooms}
     */
    roomsIdPut(body, id, callback) {
      let postBody = body;

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Rooms;

      return this.apiClient.callApi(
        '/rooms/{id}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
    /**
     * Callback function to receive the result of the roomsPost operation.
     * @callback module:api/RoomsApi~roomsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Rooms} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add new Room
     * Returns a Json with the data of the new Room
     * @param {module:api/RoomsApi~roomsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Rooms}
     */
    roomsPost(body, callback) {
      let postBody = body;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Rooms;

      return this.apiClient.callApi(
        '/rooms', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

}
