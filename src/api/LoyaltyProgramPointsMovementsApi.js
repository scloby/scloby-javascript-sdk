/**
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from "../ApiClient";
import ChainsFidelitiesMovements from '../model/ChainsFidelitiesMovements';

/**
* LoyaltyProgramPointsMovements service.
* @module api/LoyaltyProgramPointsMovementsApi
* @version 2.0.0
*/
export default class LoyaltyProgramPointsMovementsApi {

    /**
    * Constructs a new LoyaltyProgramPointsMovementsApi. 
    * @alias module:api/LoyaltyProgramPointsMovementsApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }

    /**
     * Callback function to receive the result of the fidelitiesMovementsGet operation.
     * @callback module:api/LoyaltyProgramPointsMovementsApi~fidelitiesMovementsGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ChainsFidelitiesMovements} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get All Fidelity Movements
     * Returns a Json with data about all fidelity movements of selected shop.
     * @param {Object} opts Optional parameters
     * @param {module:api/LoyaltyProgramPointsMovementsApi~fidelitiesMovementsGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ChainsFidelitiesMovements}
     */
    fidelitiesMovementsGet(opts, callback) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'pagination': opts['pagination'],
        'per_page': opts['perPage'],
        'page': opts['page']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = ChainsFidelitiesMovements;

      return this.apiClient.callApi(
        '/fidelities_movements', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
    /**
     * Callback function to receive the result of the fidelitiesMovementsIdGet operation.
     * @callback module:api/LoyaltyProgramPointsMovementsApi~fidelitiesMovementsIdGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ChainsFidelitiesMovements} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get existing Fidelity Movement
     * In this case you must specify the id in the URL
     * @param {module:api/LoyaltyProgramPointsMovementsApi~fidelitiesMovementsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ChainsFidelitiesMovements}
     */
    fidelitiesMovementsIdGet(id, callback) {
      let postBody = null;

      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = ChainsFidelitiesMovements;

      return this.apiClient.callApi(
        '/fidelities_movements/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
    /**
     * Callback function to receive the result of the fidelitiesMovementsPost operation.
     * @callback module:api/LoyaltyProgramPointsMovementsApi~fidelitiesMovementsPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ChainsFidelitiesMovements} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add new Fidelity Movement
     * Returns a Json with the data of the new Fidelity Movement
     * @param {module:api/LoyaltyProgramPointsMovementsApi~fidelitiesMovementsPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ChainsFidelitiesMovements}
     */
    fidelitiesMovementsPost(body, callback) {
      let postBody = body;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oAuth2AuthCode'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = ChainsFidelitiesMovements;

      return this.apiClient.callApi(
        '/fidelities_movements', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

}
