/**
 * Scloby Api
 * **Introduction**  **The authentication token is issued via Oauth2 standard ( https://oauth.net)**    You can apply for a Scloby test account on our developer portal **https://developer.scloby.com**   Here is your **ClientId** and **ClientSecret** for tests:    **client_id**: SclobyApiDocs    **client_secret**: DdyPNPvfPOGa0Izjct0C   **All data is JSON format, and the Content-Type header of POST/PUT request must be set to application/json.**  **Search Parameters**  You can perform a search in a collection using all first level properties in combination with some parameter, or specifying a value:    property = VALUE if you are searching for all items with a specific property value property_since = MIN_VALUE   if you are searching for all items with property value greater (or equal) than MIN_VALUE property_max   = MAX_VALUE   if you are searching for all items with property value smaller (or equal)than MAX_VALUE property_like  = LIKE_VALUE   if you are searching for a substring content You can use property_since and property_max in order to simulate **between** operator   If you are using *equal* operator, all other operators for that parameter *will be ingnored*   If you are using _max or _since operator in strings, the alphabetical ordering will be used
 *
 * OpenAPI spec version: 2.0.0
 * Contact: dev@scloby.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.SclobyApi);
  }
}(this, function(expect, SclobyApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new SclobyApi.ChainsFidelitiesMovements();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('ChainsFidelitiesMovements', function() {
    it('should create an instance of ChainsFidelitiesMovements', function() {
      // uncomment below and update the code to test ChainsFidelitiesMovements
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be.a(SclobyApi.ChainsFidelitiesMovements);
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property _date (base name: "date")', function() {
      // uncomment below and update the code to test the property _date
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property dbName (base name: "db_name")', function() {
      // uncomment below and update the code to test the property dbName
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property shopName (base name: "shop_name")', function() {
      // uncomment below and update the code to test the property shopName
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property fidelity (base name: "fidelity")', function() {
      // uncomment below and update the code to test the property fidelity
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property campaignId (base name: "campaign_id")', function() {
      // uncomment below and update the code to test the property campaignId
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property campaignName (base name: "campaign_name")', function() {
      // uncomment below and update the code to test the property campaignName
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property ruleId (base name: "rule_id")', function() {
      // uncomment below and update the code to test the property ruleId
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property ruleName (base name: "rule_name")', function() {
      // uncomment below and update the code to test the property ruleName
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property prizeId (base name: "prize_id")', function() {
      // uncomment below and update the code to test the property prizeId
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property saleId (base name: "sale_id")', function() {
      // uncomment below and update the code to test the property saleId
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property saleName (base name: "sale_name")', function() {
      // uncomment below and update the code to test the property saleName
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property itemSku (base name: "item_sku")', function() {
      // uncomment below and update the code to test the property itemSku
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property itemName (base name: "item_name")', function() {
      // uncomment below and update the code to test the property itemName
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property points (base name: "points")', function() {
      // uncomment below and update the code to test the property points
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

    it('should have the property notes (base name: "notes")', function() {
      // uncomment below and update the code to test the property notes
      //var instane = new SclobyApi.ChainsFidelitiesMovements();
      //expect(instance).to.be();
    });

  });

}));
