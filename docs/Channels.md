# SclobyApi.Channels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**name** | **String** |  | 
**imageUrl** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**enabled** | **Boolean** |  | [optional] 
**allowAutomaticPrint** | **Boolean** |  | [optional] 
**forcePaid** | **Boolean** |  | [optional] 
**addToMailReceipts** | **Boolean** |  | [optional] 
**addToNewItems** | **Boolean** |  | [optional] 
**defaultPaymentMethodId** | **Number** |  | [optional] 
