# SclobyApi.Sales

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**uuid** | **String** |  | [optional] 
**name** | **String** |  | 
**externalId** | **String** |  | [optional] 
**channel** | **String** |  | [optional] 
**saleNumber** | **Number** |  | [optional] 
**isSummary** | **Boolean** |  | [optional] 
**notes** | **String** |  | [optional] 
**saleParentId** | **Number** |  | [optional] 
**saleParentUuid** | **String** |  | [optional] 
**orderId** | **Number** |  | [optional] 
**orderUuid** | **String** |  | [optional] 
**sclobyShopId** | **String** | Scloby Pass Shop ID | [optional] 
**assignedId** | **Number** |  | [optional] 
**assignedName** | **String** |  | [optional] 
**sellerId** | **Number** |  | 
**sellerName** | **String** |  | 
**customerTaxCode** | **String** |  | [optional] 
**openAt** | **Date** |  | 
**closedAt** | **Date** | Closing (or Storing) date | [optional] 
**lastupdateAt** | **Date** |  | [optional] 
**lastupdateBy** | **Number** |  | [optional] 
**status** | **String** | Sale status, can be open, closed or stored | 
**amount** | **Number** | Sum of price * quantity + discount/surcharges of all sale_items | [optional] 
**change** | **Number** | Payment change (also known as &#x27;resto&#x27;) | [optional] 
**changeType** | **String** | Change type, can be &#x27;cash&#x27;, &#x27;ticket&#x27; or &#x27;other&#x27; | [optional] 
**finalAmount** | **Number** | Sum of price * quantity + discount/surcharges of all sale_items + discount/surcharges on amount | [optional] 
**finalNetAmount** | **Number** | Sum of final_price * quantity of all sale_items | [optional] 
**currency** | **String** |  | 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
**priceChanges** | [**[PricechangeSchema]**](PricechangeSchema.md) |  | [optional] 
**saleItems** | [**[SaleitemSchema]**](SaleitemSchema.md) |  | [optional] 
**saleCustomer** | [**SalecustomerSchema**](SalecustomerSchema.md) |  | [optional] 
**payments** | [**[SalepaymentSchema]**](SalepaymentSchema.md) |  | [optional] 
**saleDocuments** | [**[SaledocumentSchema]**](SaledocumentSchema.md) |  | [optional] 
**eInvoice** | [**SaleeinvoiceSchema**](SaleeinvoiceSchema.md) |  | [optional] 
