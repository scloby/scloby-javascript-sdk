# SclobyApi.BookingShifts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**shiftId** | **Number** |  | [optional] 
**customerId** | **Number** |  | 
**source** | **String** |  | [optional] 
**bookedFor** | **Date** |  | 
**duration** | **Number** |  | 
**status** | **String** |  | 
**people** | **Number** |  | [optional] 
**notes** | **String** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
**tags** | [**[BookingtagSchema]**](BookingtagSchema.md) |  | [optional] 
**tables** | [**[BookingtableSchema]**](BookingtableSchema.md) |  | [optional] 
**customer** | [**CustomerSchema1**](CustomerSchema1.md) |  | [optional] 
