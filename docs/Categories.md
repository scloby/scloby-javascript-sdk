# SclobyApi.Categories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** | Category name | 
**thumbnail** | **String** |  | [optional] 
**index** | **Number** | Sorting index in visualization | [optional] 
**favorite** | **Boolean** |  | [optional] 
**display** | **Boolean** | Set to true if you want to show it in orders or in cash register | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**items** | [**[ItemSchema]**](ItemSchema.md) |  | [optional] 
