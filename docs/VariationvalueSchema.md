# SclobyApi.VariationvalueSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**variationId** | **Number** |  | [optional] 
**index** | **Number** |  | [optional] 
**value** | **String** |  | [optional] 
**defaultValue** | **Boolean** |  | [optional] 
**code** | **String** |  | [optional] 
**price** | **Number** |  | [optional] 
**priceDifference** | **Number** |  | [optional] 
**imageUrl** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
