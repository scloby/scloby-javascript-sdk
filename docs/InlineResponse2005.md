# SclobyApi.InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**dbName** | **String** |  | [optional] 
**shopName** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
