# SclobyApi.VatRatesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vatGet**](VatRatesApi.md#vatGet) | **GET** /vat | Get All VAT Rates
[**vatIdDelete**](VatRatesApi.md#vatIdDelete) | **DELETE** /vat/{id} | Delete existing Vat rate
[**vatIdGet**](VatRatesApi.md#vatIdGet) | **GET** /vat/{id} | Get existing Vat
[**vatIdPut**](VatRatesApi.md#vatIdPut) | **PUT** /vat/{id} | Edit existing Vat
[**vatPost**](VatRatesApi.md#vatPost) | **POST** /vat | Add new Vat rate

<a name="vatGet"></a>
# **vatGet**
> Vat vatGet()

Get All VAT Rates

Returns a Json with data about all vat rates (&#x27;Aliquote IVA&#x27; in Italy) of selected shop.  VAT Rates can be 4 at maximum.  Not Paginated by default

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.VatRatesApi();
apiInstance.vatGet((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="vatIdDelete"></a>
# **vatIdDelete**
> vatIdDelete(id)

Delete existing Vat rate

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.VatRatesApi();
let id = "id_example"; // String | id of the Vat that need to be deleted

apiInstance.vatIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Vat that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="vatIdGet"></a>
# **vatIdGet**
> Vat vatIdGet(id)

Get existing Vat

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.VatRatesApi();
let id = "id_example"; // String | id of the vat

apiInstance.vatIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the vat | 

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="vatIdPut"></a>
# **vatIdPut**
> Vat vatIdPut(bodyid)

Edit existing Vat

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.VatRatesApi();
let body = new SclobyApi.Vat(); // Vat | Object data that need to be updated
let id = "id_example"; // String | id of the Vat that need to be updated

apiInstance.vatIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Vat**](Vat.md)| Object data that need to be updated | 
 **id** | **String**| id of the Vat that need to be updated | 

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="vatPost"></a>
# **vatPost**
> Vat vatPost(body)

Add new Vat rate

NB: You have to specify the **id** you want to assign.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.VatRatesApi();
let body = new SclobyApi.Vat(); // Vat | Vat object that needs to be added.

apiInstance.vatPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Vat**](Vat.md)| Vat object that needs to be added. | 

### Return type

[**Vat**](Vat.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

