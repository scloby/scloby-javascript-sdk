# SclobyApi.VariationSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | 
**required** | **Boolean** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**itemId** | **Number** |  | [optional] 
**variationValues** | [**[VariationvalueSchema]**](VariationvalueSchema.md) |  | [optional] 
