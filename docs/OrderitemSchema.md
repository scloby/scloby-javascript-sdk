# SclobyApi.OrderitemSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**uuid** | **String** |  | 
**orderId** | **Number** |  | [optional] 
**itemId** | **Number** |  | [optional] 
**name** | **String** | Original Name of item sold | [optional] 
**orderName** | **String** | Name you want to print into kitchen tickets | [optional] 
**categoryId** | **Number** |  | [optional] 
**categoryName** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**halfPortion** | **Boolean** |  | [optional] 
**price** | **Number** |  | 
**cost** | **Number** |  | [optional] 
**netPrice** | **Number** |  | 
**vatPerc** | **Number** |  | 
**finalPrice** | **Number** | Price with VAT (ingredients or variations price differences are included) | 
**finalNetPrice** | **Number** | Price without VAT (ingredients or variations price differences are included) | 
**quantity** | **Number** |  | 
**unit** | **String** |  | [optional] 
**exit** | **Number** | Item exit, can be null or from 1 to 10 | [optional] 
**lastupdateAt** | **Date** |  | [optional] 
**lastupdateBy** | **Number** |  | [optional] 
**addedAt** | **Date** |  | [optional] 
**operatorId** | **Number** |  | 
**operatorName** | **String** |  | 
**departmentId** | **Number** |  | [optional] 
**departmentName** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**variations** | [**[OrderitemvariationSchema]**](OrderitemvariationSchema.md) |  | [optional] 
**ingredients** | [**[OrderitemingredientSchema]**](OrderitemingredientSchema.md) |  | [optional] 
