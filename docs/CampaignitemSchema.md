# SclobyApi.CampaignitemSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | 
**sku** | **String** |  | 
**points** | **Number** |  | 
**campaignId** | **Number** |  | [optional] 
