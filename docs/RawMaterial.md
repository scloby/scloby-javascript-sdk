# SclobyApi.RawMaterial

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** | Raw Material name | 
**cost** | **Number** |  | [optional] 
**stockType** | **String** |  | [optional] 
**autoUnload** | **Boolean** |  | [optional] 
**quantityAlert** | **Number** |  | [optional] 
**unit** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
**defaultSupplierId** | **Number** |  | [optional] 
