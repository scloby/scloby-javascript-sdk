# SclobyApi.InlineResponse2003Shop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**uuid** | **String** |  | [optional] 
