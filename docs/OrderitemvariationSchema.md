# SclobyApi.OrderitemvariationSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**orderItemId** | **Number** |  | [optional] 
**name** | **String** | Name of variation, for example Size | 
**value** | **String** | Value of a variant, for example XL | 
**variationId** | **Number** |  | 
**variationValueId** | **Number** |  | 
**priceDifference** | **Number** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
