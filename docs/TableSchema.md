# SclobyApi.TableSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | 
**orderType** | **String** |  | 
**shape** | **String** |  | 
**xpos** | **Number** |  | 
**ypos** | **Number** |  | 
**width** | **Number** |  | 
**height** | **Number** |  | 
**covers** | **Number** |  | 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**roomId** | **Number** |  | [optional] 
