# SclobyApi.ChainsFidelitiesMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**_date** | **Date** |  | 
**dbName** | **String** |  | 
**shopName** | **String** |  | [optional] 
**fidelity** | **String** |  | 
**campaignId** | **Number** |  | 
**campaignName** | **String** |  | 
**ruleId** | **Number** |  | [optional] 
**ruleName** | **String** |  | [optional] 
**prizeId** | **Number** |  | [optional] 
**saleId** | **Number** |  | [optional] 
**saleName** | **String** |  | [optional] 
**itemSku** | **String** |  | [optional] 
**itemName** | **String** |  | [optional] 
**points** | **Number** |  | 
**notes** | **String** |  | [optional] 
