# SclobyApi.DepartmentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**name** | **String** |  | 
**lawRef** | **String** |  | [optional] 
**vatId** | **Number** |  | 
**notDiscountable** | **Boolean** |  | [optional] 
**vat** | [**VatSchema**](VatSchema.md) |  | [optional] 
