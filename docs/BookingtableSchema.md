# SclobyApi.BookingtableSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**bookingId** | **Number** |  | [optional] 
**tableId** | **String** |  | [optional] 
