# SclobyApi.InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weekdaysPeriod** | **[String]** |  | [optional] 
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**startTime** | **String** |  | [optional] 
**endTime** | **String** |  | [optional] 
**startPeriod** | **String** |  | [optional] 
**endPeriod** | **String** |  | [optional] 
**defaultDuration** | **Number** |  | [optional] 
**defaultPricelist** | **Number** |  | [optional] 
**instoreSeatsLimit** | **Number** |  | [optional] 
**onlineSeatsLimit** | **Number** |  | [optional] 
**roomRestrictions** | **[String]** |  | [optional] 
