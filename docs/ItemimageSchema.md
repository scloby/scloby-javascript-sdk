# SclobyApi.ItemimageSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**imageUrl** | **String** |  | 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**itemId** | **Number** |  | [optional] 
