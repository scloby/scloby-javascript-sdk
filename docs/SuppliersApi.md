# SclobyApi.SuppliersApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suppliersGet**](SuppliersApi.md#suppliersGet) | **GET** /suppliers | GET All suppliers
[**suppliersIdDelete**](SuppliersApi.md#suppliersIdDelete) | **DELETE** /suppliers/{id} | Delete existing supplier
[**suppliersIdPut**](SuppliersApi.md#suppliersIdPut) | **PUT** /suppliers/{id} | Edit existing supplier
[**suppliersPost**](SuppliersApi.md#suppliersPost) | **POST** /suppliers | Add a new supplier

<a name="suppliersGet"></a>
# **suppliersGet**
> Supplier suppliersGet(opts)

GET All suppliers

Returns a Json with data about all suppliers of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.SuppliersApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.suppliersGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**Supplier**](Supplier.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="suppliersIdDelete"></a>
# **suppliersIdDelete**
> suppliersIdDelete(id)

Delete existing supplier

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.SuppliersApi();
let id = "id_example"; // String | id of the supplier that need to be deleted

apiInstance.suppliersIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the supplier that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="suppliersIdPut"></a>
# **suppliersIdPut**
> Supplier suppliersIdPut(bodyid)

Edit existing supplier

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.SuppliersApi();
let body = new SclobyApi.Supplier(); // Supplier | Object data that need to be updated
let id = "id_example"; // String | id of the supplier that need to be updated

apiInstance.suppliersIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Supplier**](Supplier.md)| Object data that need to be updated | 
 **id** | **String**| id of the supplier that need to be updated | 

### Return type

[**Supplier**](Supplier.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="suppliersPost"></a>
# **suppliersPost**
> Supplier suppliersPost(body)

Add a new supplier

Returns a Json with the data of the new supplier

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.SuppliersApi();
let body = new SclobyApi.Supplier(); // Supplier | Supplier object that needs to be added to the selected shop.

apiInstance.suppliersPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Supplier**](Supplier.md)| Supplier object that needs to be added to the selected shop. | 

### Return type

[**Supplier**](Supplier.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

