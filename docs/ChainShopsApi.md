# SclobyApi.ChainShopsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**chainShopsIdGet**](ChainShopsApi.md#chainShopsIdGet) | **GET** /chain_shops/{id} | Get existing Chain Shop

<a name="chainShopsIdGet"></a>
# **chainShopsIdGet**
> InlineResponse2005 chainShopsIdGet(id)

Get existing Chain Shop

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ChainShopsApi();
let id = "id_example"; // String | id of the Shop

apiInstance.chainShopsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Shop | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

