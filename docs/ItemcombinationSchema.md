# SclobyApi.ItemcombinationSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**itemId** | **Number** |  | [optional] 
**price1** | **Number** |  | 
**price2** | **Number** |  | [optional] 
**price3** | **Number** |  | [optional] 
**price4** | **Number** |  | [optional] 
**price5** | **Number** |  | [optional] 
**sku** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**combinationValues** | [**[VariationvalueSchema]**](VariationvalueSchema.md) |  | [optional] 
**barcodes** | [**[CombinationbarcodeSchema]**](CombinationbarcodeSchema.md) |  | [optional] 
