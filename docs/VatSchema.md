# SclobyApi.VatSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**code** | **String** |  | [optional] 
**value** | **Number** |  | 
