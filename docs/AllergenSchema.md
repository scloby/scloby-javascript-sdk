# SclobyApi.AllergenSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** | Allergen name | 
**description** | **String** |  | [optional] 
