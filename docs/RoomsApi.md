# SclobyApi.RoomsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**roomsGet**](RoomsApi.md#roomsGet) | **GET** /rooms | Get All Rooms
[**roomsIdDelete**](RoomsApi.md#roomsIdDelete) | **DELETE** /rooms/{id} | Delete existing Room
[**roomsIdGet**](RoomsApi.md#roomsIdGet) | **GET** /rooms/{id} | Get existing Printer
[**roomsIdPut**](RoomsApi.md#roomsIdPut) | **PUT** /rooms/{id} | Edit existing Room
[**roomsPost**](RoomsApi.md#roomsPost) | **POST** /rooms | Add new Room

<a name="roomsGet"></a>
# **roomsGet**
> Rooms roomsGet(opts)

Get All Rooms

Returns a Json with data about all rooms of selected shop.  Paginated by default (per_page&#x3D;1000)

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RoomsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.roomsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="roomsIdDelete"></a>
# **roomsIdDelete**
> roomsIdDelete(id)

Delete existing Room

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RoomsApi();
let id = "id_example"; // String | id of the Room that need to be deleted

apiInstance.roomsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Room that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="roomsIdGet"></a>
# **roomsIdGet**
> Rooms roomsIdGet(id)

Get existing Printer

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RoomsApi();
let id = "id_example"; // String | id of the Printer

apiInstance.roomsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Printer | 

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="roomsIdPut"></a>
# **roomsIdPut**
> Rooms roomsIdPut(bodyid)

Edit existing Room

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RoomsApi();
let body = new SclobyApi.Rooms(); // Rooms | Object data that need to be updated
let id = "id_example"; // String | id of the Room that need to be updated

apiInstance.roomsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Rooms**](Rooms.md)| Object data that need to be updated | 
 **id** | **String**| id of the Room that need to be updated | 

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="roomsPost"></a>
# **roomsPost**
> Rooms roomsPost(body)

Add new Room

Returns a Json with the data of the new Room

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RoomsApi();
let body = new SclobyApi.Rooms(); // Rooms | Room object that needs to be added.

apiInstance.roomsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Rooms**](Rooms.md)| Room object that needs to be added. | 

### Return type

[**Rooms**](Rooms.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

