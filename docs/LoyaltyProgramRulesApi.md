# SclobyApi.LoyaltyProgramRulesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rulesGet**](LoyaltyProgramRulesApi.md#rulesGet) | **GET** /rules | Get All Rules
[**rulesIdDelete**](LoyaltyProgramRulesApi.md#rulesIdDelete) | **DELETE** /rules/{id} | Delete existing Rule
[**rulesIdGet**](LoyaltyProgramRulesApi.md#rulesIdGet) | **GET** /rules/{id} | Get existing Rule
[**rulesIdPut**](LoyaltyProgramRulesApi.md#rulesIdPut) | **PUT** /rules/{id} | Edit existing Rule
[**rulesPost**](LoyaltyProgramRulesApi.md#rulesPost) | **POST** /rules | Add new Rule

<a name="rulesGet"></a>
# **rulesGet**
> ChainsRules rulesGet(opts)

Get All Rules

Returns a Json with data about all rules of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramRulesApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.rulesGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rulesIdDelete"></a>
# **rulesIdDelete**
> rulesIdDelete(id)

Delete existing Rule

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramRulesApi();
let id = "id_example"; // String | id of the Rule that need to be deleted

apiInstance.rulesIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Rule that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="rulesIdGet"></a>
# **rulesIdGet**
> ChainsRules rulesIdGet(id)

Get existing Rule

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramRulesApi();
let id = "id_example"; // String | id of the Rule

apiInstance.rulesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Rule | 

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rulesIdPut"></a>
# **rulesIdPut**
> ChainsRules rulesIdPut(bodyid)

Edit existing Rule

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramRulesApi();
let body = new SclobyApi.ChainsRules(); // ChainsRules | Object data that need to be updated
let id = "id_example"; // String | id of the Rule that need to be updated

apiInstance.rulesIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsRules**](ChainsRules.md)| Object data that need to be updated | 
 **id** | **String**| id of the Rule that need to be updated | 

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rulesPost"></a>
# **rulesPost**
> ChainsRules rulesPost(body)

Add new Rule

Returns a Json with the data of the new Rule

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramRulesApi();
let body = new SclobyApi.ChainsRules(); // ChainsRules | Rule object that needs to be added.

apiInstance.rulesPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsRules**](ChainsRules.md)| Rule object that needs to be added. | 

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

