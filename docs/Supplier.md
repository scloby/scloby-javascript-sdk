# SclobyApi.Supplier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | 
**thumbnail** | **String** |  | [optional] 
**addressStreet** | **String** |  | [optional] 
**addressNumber** | **String** |  | [optional] 
**addressCity** | **String** |  | [optional] 
**addressZip** | **String** |  | [optional] 
**addressProv** | **String** |  | [optional] 
**phone1** | **String** |  | [optional] 
**phone2** | **String** |  | [optional] 
**fax** | **String** |  | [optional] 
**email1** | **String** |  | [optional] 
**email2** | **String** |  | [optional] 
**emailPec** | **String** |  | [optional] 
**website** | **String** |  | [optional] 
**vatCode** | **String** |  | 
**notes** | **String** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
