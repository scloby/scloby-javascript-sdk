# SclobyApi.LoyaltyProgramCampaignsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**campaignsGet**](LoyaltyProgramCampaignsApi.md#campaignsGet) | **GET** /campaigns | Get All Campaigns
[**campaignsIdDelete**](LoyaltyProgramCampaignsApi.md#campaignsIdDelete) | **DELETE** /campaigns/{id} | Delete existing Campaign
[**campaignsIdGet**](LoyaltyProgramCampaignsApi.md#campaignsIdGet) | **GET** /campaigns/{id} | Get existing Campaign
[**campaignsIdPut**](LoyaltyProgramCampaignsApi.md#campaignsIdPut) | **PUT** /campaigns/{id} | Edit existing Campaign
[**campaignsPost**](LoyaltyProgramCampaignsApi.md#campaignsPost) | **POST** /campaigns | Add new Campaign

<a name="campaignsGet"></a>
# **campaignsGet**
> ChainsCampaigns campaignsGet(opts)

Get All Campaigns

Returns a Json with data about all campaigns of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramCampaignsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.campaignsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="campaignsIdDelete"></a>
# **campaignsIdDelete**
> campaignsIdDelete(id)

Delete existing Campaign

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramCampaignsApi();
let id = "id_example"; // String | id of the Campaign that need to be deleted

apiInstance.campaignsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Campaign that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="campaignsIdGet"></a>
# **campaignsIdGet**
> ChainsCampaigns campaignsIdGet(id)

Get existing Campaign

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramCampaignsApi();
let id = "id_example"; // String | id of the Campaign

apiInstance.campaignsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Campaign | 

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="campaignsIdPut"></a>
# **campaignsIdPut**
> ChainsCampaigns campaignsIdPut(bodyid)

Edit existing Campaign

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramCampaignsApi();
let body = new SclobyApi.ChainsCampaigns(); // ChainsCampaigns | Object data that need to be updated
let id = "id_example"; // String | id of the Campaign that need to be updated

apiInstance.campaignsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsCampaigns**](ChainsCampaigns.md)| Object data that need to be updated | 
 **id** | **String**| id of the Campaign that need to be updated | 

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="campaignsPost"></a>
# **campaignsPost**
> ChainsCampaigns campaignsPost(body)

Add new Campaign

Returns a Json with the data of the new Campaign

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramCampaignsApi();
let body = new SclobyApi.ChainsCampaigns(); // ChainsCampaigns | Campaign object that needs to be added.

apiInstance.campaignsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsCampaigns**](ChainsCampaigns.md)| Campaign object that needs to be added. | 

### Return type

[**ChainsCampaigns**](ChainsCampaigns.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

