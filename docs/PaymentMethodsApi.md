# SclobyApi.PaymentMethodsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentMethodsGet**](PaymentMethodsApi.md#paymentMethodsGet) | **GET** /payment_methods | Get All Payment Methods
[**paymentMethodsIdDelete**](PaymentMethodsApi.md#paymentMethodsIdDelete) | **DELETE** /payment_methods/{id} | Delete existing Vat rate
[**paymentMethodsIdGet**](PaymentMethodsApi.md#paymentMethodsIdGet) | **GET** /payment_methods/{id} | Get existing Payment Method
[**paymentMethodsIdPut**](PaymentMethodsApi.md#paymentMethodsIdPut) | **PUT** /payment_methods/{id} | Edit existing Payment Method
[**paymentMethodsPost**](PaymentMethodsApi.md#paymentMethodsPost) | **POST** /payment_methods | Add new Payment Method

<a name="paymentMethodsGet"></a>
# **paymentMethodsGet**
> PaymentMethods paymentMethodsGet(opts)

Get All Payment Methods

Returns a Json with data about all payment methods of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PaymentMethodsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.paymentMethodsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentMethodsIdDelete"></a>
# **paymentMethodsIdDelete**
> paymentMethodsIdDelete(id)

Delete existing Vat rate

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PaymentMethodsApi();
let id = "id_example"; // String | id of the Payment method that need to be deleted

apiInstance.paymentMethodsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Payment method that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="paymentMethodsIdGet"></a>
# **paymentMethodsIdGet**
> PaymentMethods paymentMethodsIdGet(id)

Get existing Payment Method

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PaymentMethodsApi();
let id = "id_example"; // String | id of the Payment Method

apiInstance.paymentMethodsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Payment Method | 

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentMethodsIdPut"></a>
# **paymentMethodsIdPut**
> PaymentMethods paymentMethodsIdPut(bodyid)

Edit existing Payment Method

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PaymentMethodsApi();
let body = new SclobyApi.PaymentMethods(); // PaymentMethods | Object data that need to be updated
let id = "id_example"; // String | id of the Payment Method that need to be updated

apiInstance.paymentMethodsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PaymentMethods**](PaymentMethods.md)| Object data that need to be updated | 
 **id** | **String**| id of the Payment Method that need to be updated | 

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="paymentMethodsPost"></a>
# **paymentMethodsPost**
> PaymentMethods paymentMethodsPost(body)

Add new Payment Method

Returns a Json with the data of the new Payment Method

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PaymentMethodsApi();
let body = new SclobyApi.PaymentMethods(); // PaymentMethods | Payment Method object that needs to be added.

apiInstance.paymentMethodsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PaymentMethods**](PaymentMethods.md)| Payment Method object that needs to be added. | 

### Return type

[**PaymentMethods**](PaymentMethods.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

