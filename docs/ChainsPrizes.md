# SclobyApi.ChainsPrizes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**campaignId** | **Number** |  | 
**name** | **String** |  | 
**points** | **Number** |  | 
**type** | **String** |  | 
**discountAmount** | **Number** |  | [optional] 
**itemSku** | **String** |  | [optional] 
**validFrom** | **Date** |  | [optional] 
**validTo** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
