# SclobyApi.Bookings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**shiftId** | **Number** |  | [optional] 
**customerId** | **Number** |  | 
**source** | **String** |  | [optional] 
**bookedFor** | **Date** |  | 
**duration** | **Number** |  | 
**status** | **String** |  | 
**people** | **Number** |  | [optional] 
**notes** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
**tags** | [**[BookingtagSchema]**](BookingtagSchema.md) |  | [optional] 
**tables** | [**[BookingtableSchema]**](BookingtableSchema.md) |  | [optional] 
**customer** | [**CustomerSchema**](CustomerSchema.md) |  | [optional] 
