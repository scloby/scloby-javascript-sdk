# SclobyApi.Orders

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**uuid** | **String** |  | [optional] 
**orderParentId** | **Number** |  | [optional] 
**orderParentUuid** | **String** |  | [optional] 
**name** | **String** |  | 
**type** | **String** | Order type, can be normal or take_away | 
**paid** | **Boolean** |  | [optional] 
**externalId** | **String** |  | [optional] 
**channel** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**status** | **String** | Order status, can be open, checkout, closed or missed | 
**amount** | **Number** | Sum of final_price of all order_items | [optional] 
**netAmount** | **Number** | Sum of final_net_price of all order_items | [optional] 
**roomId** | **Number** |  | [optional] 
**roomName** | **String** |  | [optional] 
**tableId** | **Number** |  | [optional] 
**tableName** | **String** |  | [optional] 
**operatorId** | **Number** |  | 
**operatorName** | **String** |  | 
**lastupdateAt** | **Date** |  | [optional] 
**lastupdateBy** | **Number** |  | [optional] 
**orderNumber** | **Number** |  | [optional] 
**covers** | **Number** |  | [optional] 
**openAt** | **Date** |  | 
**checkoutAt** | **Date** |  | [optional] 
**closedAt** | **Date** |  | [optional] 
**deliverAt** | **Date** |  | [optional] 
**lastSentBy** | **Number** |  | [optional] 
**lastSentAt** | **Date** |  | [optional] 
**previous** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
**previousOrder** | **Object** |  | [optional] 
**orderItems** | [**[OrderitemSchema]**](OrderitemSchema.md) |  | [optional] 
**orderCustomer** | [**OrdercustomerSchema**](OrdercustomerSchema.md) |  | [optional] 
