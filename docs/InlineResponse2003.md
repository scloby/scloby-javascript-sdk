# SclobyApi.InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**username** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**pin** | **String** |  | [optional] 
**thumbnail** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**role** | **String** |  | [optional] 
**shop** | [**InlineResponse2003Shop**](InlineResponse2003Shop.md) |  | [optional] 
