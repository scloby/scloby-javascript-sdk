# SclobyApi.ItembomcomponentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**quantity** | **Number** |  | 
**name** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**componentItemId** | **Number** |  | [optional] 
**componentRawMaterialId** | **Number** |  | [optional] 
**bomComponentId** | **Number** |  | [optional] 
**itemId** | **Number** |  | [optional] 
