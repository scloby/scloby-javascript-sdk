# SclobyApi.InlineResponse2004

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**campaignId** | **Number** |  | [optional] 
**fidelity** | **String** |  | [optional] 
**points** | **Number** |  | [optional] 
**lastupdateAt** | **Date** |  | [optional] 
