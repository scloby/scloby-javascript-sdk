# SclobyApi.RawMaterialsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rawMaterialsGet**](RawMaterialsApi.md#rawMaterialsGet) | **GET** /raw_materials | Get All Raw Materials
[**rawMaterialsIdDelete**](RawMaterialsApi.md#rawMaterialsIdDelete) | **DELETE** /raw_materials/{id} | Delete existing Raw material
[**rawMaterialsIdGet**](RawMaterialsApi.md#rawMaterialsIdGet) | **GET** /raw_materials/{id} | Get existing raw Material
[**rawMaterialsIdPut**](RawMaterialsApi.md#rawMaterialsIdPut) | **PUT** /raw_materials/{id} | Edit existing Raw material
[**rawMaterialsPost**](RawMaterialsApi.md#rawMaterialsPost) | **POST** /raw_materials | Add new Raw material

<a name="rawMaterialsGet"></a>
# **rawMaterialsGet**
> RawMaterial rawMaterialsGet(opts)

Get All Raw Materials

Return a JSON with data about all Raw materials

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RawMaterialsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.rawMaterialsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rawMaterialsIdDelete"></a>
# **rawMaterialsIdDelete**
> rawMaterialsIdDelete(id)

Delete existing Raw material

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RawMaterialsApi();
let id = "id_example"; // String | id of the raw material that need to be deleted

apiInstance.rawMaterialsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the raw material that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="rawMaterialsIdGet"></a>
# **rawMaterialsIdGet**
> RawMaterial rawMaterialsIdGet(id)

Get existing raw Material

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RawMaterialsApi();
let id = "id_example"; // String | id of the Raw material

apiInstance.rawMaterialsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Raw material | 

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rawMaterialsIdPut"></a>
# **rawMaterialsIdPut**
> RawMaterial rawMaterialsIdPut(bodyid)

Edit existing Raw material

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RawMaterialsApi();
let body = new SclobyApi.RawMaterial(); // RawMaterial | Object data that need to be updated
let id = "id_example"; // String | id of the raw material that need to be updated

apiInstance.rawMaterialsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RawMaterial**](RawMaterial.md)| Object data that need to be updated | 
 **id** | **String**| id of the raw material that need to be updated | 

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rawMaterialsPost"></a>
# **rawMaterialsPost**
> RawMaterial rawMaterialsPost(body)

Add new Raw material

Returns a Json with the data of the new raw material

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.RawMaterialsApi();
let body = new SclobyApi.RawMaterial(); // RawMaterial | Item object that needs to be added.

apiInstance.rawMaterialsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RawMaterial**](RawMaterial.md)| Item object that needs to be added. | 

### Return type

[**RawMaterial**](RawMaterial.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

