# SclobyApi.RuleSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | 
**type** | **String** |  | 
**minThreshold** | **Number** |  | [optional] 
**maxThreshold** | **Number** |  | [optional] 
**excludeToMin** | **Boolean** |  | [optional] 
**points** | **Number** |  | 
**param1** | **String** |  | [optional] 
**param2** | **String** |  | [optional] 
**param3** | **String** |  | [optional] 
