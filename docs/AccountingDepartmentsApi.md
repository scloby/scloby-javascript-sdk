# SclobyApi.AccountingDepartmentsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**departmentsGet**](AccountingDepartmentsApi.md#departmentsGet) | **GET** /departments | Get All Departments
[**departmentsIdDelete**](AccountingDepartmentsApi.md#departmentsIdDelete) | **DELETE** /departments/{id} | Delete existing Department
[**departmentsIdGet**](AccountingDepartmentsApi.md#departmentsIdGet) | **GET** /departments/{id} | Get existing department
[**departmentsIdPut**](AccountingDepartmentsApi.md#departmentsIdPut) | **PUT** /departments/{id} | Edit existing Department
[**departmentsPost**](AccountingDepartmentsApi.md#departmentsPost) | **POST** /departments | Add new Department

<a name="departmentsGet"></a>
# **departmentsGet**
> Departments departmentsGet(opts)

Get All Departments

Returns a Json with data about all Departments (&#x27;Reparti&#x27; in Italy) of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.AccountingDepartmentsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.departmentsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="departmentsIdDelete"></a>
# **departmentsIdDelete**
> departmentsIdDelete(id)

Delete existing Department

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.AccountingDepartmentsApi();
let id = "id_example"; // String | id of the Department that need to be deleted

apiInstance.departmentsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Department that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="departmentsIdGet"></a>
# **departmentsIdGet**
> Departments departmentsIdGet(id)

Get existing department

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.AccountingDepartmentsApi();
let id = "id_example"; // String | id of the department

apiInstance.departmentsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the department | 

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="departmentsIdPut"></a>
# **departmentsIdPut**
> Departments departmentsIdPut(bodyid)

Edit existing Department

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.AccountingDepartmentsApi();
let body = new SclobyApi.StockMovements(); // StockMovements | Object data that need to be updated
let id = "id_example"; // String | id of the department that need to be updated

apiInstance.departmentsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StockMovements**](StockMovements.md)| Object data that need to be updated | 
 **id** | **String**| id of the department that need to be updated | 

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="departmentsPost"></a>
# **departmentsPost**
> Departments departmentsPost(body)

Add new Department

Each Department **require an associated VAT Rate**. First of add or edit a department, you must know the associated VAT Rate ID.  NB: You have to specify the **id** you want to assign, you can have at maximum 10 departments (id max 10).

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.AccountingDepartmentsApi();
let body = new SclobyApi.Departments(); // Departments | Department object that needs to be added.

apiInstance.departmentsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Departments**](Departments.md)| Department object that needs to be added. | 

### Return type

[**Departments**](Departments.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

