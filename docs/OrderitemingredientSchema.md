# SclobyApi.OrderitemingredientSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**orderItemId** | **Number** |  | [optional] 
**name** | **String** |  | 
**type** | **String** | Type of ingredient variation, can be added or removed | [optional] 
**quantity** | **Number** |  | [optional] 
**ingredientId** | **Number** |  | 
**priceDifference** | **Number** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
