# SclobyApi.InlineResponse2012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**campaignId** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**points** | **Number** |  | [optional] 
**type** | **String** |  | [optional] 
**discountAmount** | **Number** |  | [optional] 
**itemSku** | **String** |  | [optional] 
**validFrom** | **Date** |  | [optional] 
**validTo** | **Date** |  | [optional] 
