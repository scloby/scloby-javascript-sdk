# SclobyApi.SalepaymentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**saleId** | **Number** |  | [optional] 
**paymentMethodId** | **Number** |  | 
**paymentMethodName** | **String** |  | 
**paymentMethodTypeId** | **Number** |  | 
**paymentMethodTypeName** | **String** |  | 
**unclaimed** | **Boolean** |  | [optional] 
**amount** | **Number** |  | 
**paid** | **Boolean** |  | [optional] 
**_date** | **Date** |  | [optional] 
**acquirerName** | **String** |  | [optional] 
**acquirerId** | **Number** |  | [optional] 
**paymentData** | **String** |  | [optional] 
**trx** | **String** |  | [optional] 
**calltrx** | **String** |  | [optional] 
**cardCircuitId** | **Number** |  | [optional] 
**cardCircuitName** | **String** |  | [optional] 
**ticketCircuit** | **String** |  | [optional] 
**ticketName** | **String** |  | [optional] 
**ticketId** | **Number** |  | [optional] 
**code** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
