# SclobyApi.ComponentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** | Component name | 
**price** | **Number** |  | [optional] 
**priceDifference** | **Number** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
