# SclobyApi.BookingtagSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**bookingId** | **Number** |  | [optional] 
**tag** | **String** |  | [optional] 
