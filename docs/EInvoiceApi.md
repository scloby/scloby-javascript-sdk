# SclobyApi.EInvoiceApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesEInvoiceCheckPost**](EInvoiceApi.md#salesEInvoiceCheckPost) | **POST** /sales/e_invoice/check | Check e-invoice status
[**sendEInvoiceIdGet**](EInvoiceApi.md#sendEInvoiceIdGet) | **GET** /send_e_invoice/{id} | Send e-invoice

<a name="salesEInvoiceCheckPost"></a>
# **salesEInvoiceCheckPost**
> InlineResponse2002 salesEInvoiceCheckPost(body)

Check e-invoice status

Return a JSON of the sale

### Example
```javascript
import SclobyApi from 'scloby_api';

let apiInstance = new SclobyApi.EInvoiceApi();
let body = new SclobyApi.Sales(); // Sales | Object of the Sale

apiInstance.salesEInvoiceCheckPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Sales**](Sales.md)| Object of the Sale | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="sendEInvoiceIdGet"></a>
# **sendEInvoiceIdGet**
> Sales sendEInvoiceIdGet(id)

Send e-invoice

In this case you must specify the id of the sale in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.EInvoiceApi();
let id = "id_example"; // String | id of the sale

apiInstance.sendEInvoiceIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the sale | 

### Return type

[**Sales**](Sales.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

