# SclobyApi.PaymentMethods

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**name** | **String** |  | 
**paymentMethodTypeId** | **Number** |  | 
**enableSum** | **Boolean** |  | 
**unclaimed** | **Boolean** |  | 
**hidden** | **Boolean** |  | [optional] 
**bundleName** | **String** |  | [optional] 
**schemaName** | **String** |  | [optional] 
