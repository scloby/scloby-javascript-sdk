# SclobyApi.ShiftsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bookingShiftsGet**](ShiftsApi.md#bookingShiftsGet) | **GET** /booking_shifts | GET All Shifts
[**bookingShiftsIdDelete**](ShiftsApi.md#bookingShiftsIdDelete) | **DELETE** /booking_shifts/{id} | Delete existing shift
[**bookingShiftsIdGet**](ShiftsApi.md#bookingShiftsIdGet) | **GET** /booking_shifts/{id} | Get existing Shift
[**bookingShiftsIdPut**](ShiftsApi.md#bookingShiftsIdPut) | **PUT** /booking_shifts/{id} | Edit existing shift
[**bookingShiftsPost**](ShiftsApi.md#bookingShiftsPost) | **POST** /booking_shifts | Add Shift

<a name="bookingShiftsGet"></a>
# **bookingShiftsGet**
> BookingShifts bookingShiftsGet(opts)

GET All Shifts

Returns a Json with data about all Shifts of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ShiftsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.bookingShiftsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**BookingShifts**](BookingShifts.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="bookingShiftsIdDelete"></a>
# **bookingShiftsIdDelete**
> bookingShiftsIdDelete(id)

Delete existing shift

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ShiftsApi();
let id = "id_example"; // String | id of the shift that need to be deleted

apiInstance.bookingShiftsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the shift that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="bookingShiftsIdGet"></a>
# **bookingShiftsIdGet**
> BookingShifts bookingShiftsIdGet(id)

Get existing Shift

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ShiftsApi();
let id = "id_example"; // String | id of the shift

apiInstance.bookingShiftsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the shift | 

### Return type

[**BookingShifts**](BookingShifts.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="bookingShiftsIdPut"></a>
# **bookingShiftsIdPut**
> InlineResponse200 bookingShiftsIdPut(bodyid)

Edit existing shift

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ShiftsApi();
let body = new SclobyApi.BookingShifts(); // BookingShifts | Object data that need to be updated
let id = "id_example"; // String | id of the shift that need to be updated

apiInstance.bookingShiftsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BookingShifts**](BookingShifts.md)| Object data that need to be updated | 
 **id** | **String**| id of the shift that need to be updated | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="bookingShiftsPost"></a>
# **bookingShiftsPost**
> BookingShifts bookingShiftsPost(body)

Add Shift

Returns a Json with the data of the new shift

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ShiftsApi();
let body = new SclobyApi.BookingShifts(); // BookingShifts | Shift object that needs to be added to the selected shop.

apiInstance.bookingShiftsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BookingShifts**](BookingShifts.md)| Shift object that needs to be added to the selected shop. | 

### Return type

[**BookingShifts**](BookingShifts.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

