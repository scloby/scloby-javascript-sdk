# SclobyApi.PrepaidCreditMovementsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**prepaidCustomersGet**](PrepaidCreditMovementsApi.md#prepaidCustomersGet) | **GET** /prepaid_customers | Get list of Prepaid Customers
[**prepaidMovementsIdGet**](PrepaidCreditMovementsApi.md#prepaidMovementsIdGet) | **GET** /prepaid_movements/{id} | Get existing Prepaid Movement
[**prepaidMovementsPost**](PrepaidCreditMovementsApi.md#prepaidMovementsPost) | **POST** /prepaid_movements | Add new Prepaid Movement

<a name="prepaidCustomersGet"></a>
# **prepaidCustomersGet**
> prepaidCustomersGet(opts)

Get list of Prepaid Customers

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PrepaidCreditMovementsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.prepaidCustomersGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="prepaidMovementsIdGet"></a>
# **prepaidMovementsIdGet**
> ChainPrepaidMovements prepaidMovementsIdGet(id)

Get existing Prepaid Movement

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PrepaidCreditMovementsApi();
let id = "id_example"; // String | id of the Prepaid Movement

apiInstance.prepaidMovementsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Prepaid Movement | 

### Return type

[**ChainPrepaidMovements**](ChainPrepaidMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="prepaidMovementsPost"></a>
# **prepaidMovementsPost**
> ChainPrepaidMovements prepaidMovementsPost(body, opts)

Add new Prepaid Movement

Returns a Json with the data of the new Prepaid Movement

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.PrepaidCreditMovementsApi();
let body = new SclobyApi.ChainPrepaidMovements(); // ChainPrepaidMovements | Prepaid Movement object that needs to be added.
let opts = { 
  'pagination': true // Boolean | Pagination parameter
  'perPage': 56 // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.prepaidMovementsPost(body, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainPrepaidMovements**](ChainPrepaidMovements.md)| Prepaid Movement object that needs to be added. | 
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**ChainPrepaidMovements**](ChainPrepaidMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

