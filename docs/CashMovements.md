# SclobyApi.CashMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**_date** | **Date** |  | 
**type** | **String** |  | 
**description** | **String** |  | [optional] 
**paymentMethodName** | **String** |  | [optional] 
**cardCircuitName** | **String** |  | [optional] 
**amount** | **Number** |  | 
**saleId** | **Number** |  | [optional] 
**account** | **String** |  | 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
