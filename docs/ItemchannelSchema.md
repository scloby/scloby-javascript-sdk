# SclobyApi.ItemchannelSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**channelId** | **String** |  | 
**defaultPricelist** | **Number** |  | [optional] 
**categoryId** | **Number** |  | [optional] 
**itemId** | **Number** |  | [optional] 
