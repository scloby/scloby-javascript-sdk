# SclobyApi.LoyaltyProgramCustomerPointsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fidelitiesPointsIdGet**](LoyaltyProgramCustomerPointsApi.md#fidelitiesPointsIdGet) | **GET** /fidelities_points/{id} | Get existing Fidelity Point

<a name="fidelitiesPointsIdGet"></a>
# **fidelitiesPointsIdGet**
> InlineResponse2004 fidelitiesPointsIdGet(id)

Get existing Fidelity Point

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramCustomerPointsApi();
let id = "id_example"; // String | id of the Fidelity Point

apiInstance.fidelitiesPointsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Fidelity Point | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

