# SclobyApi.SaleitemSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**externalId** | **String** |  | [optional] 
**saleId** | **Number** |  | [optional] 
**referenceSequentialNumber** | **Number** |  | [optional] 
**referenceText** | **String** |  | [optional] 
**referenceDate** | **Date** |  | [optional] 
**uuid** | **String** |  | 
**itemId** | **Number** |  | [optional] 
**combinationId** | **Number** |  | [optional] 
**prizeId** | **Number** |  | [optional] 
**type** | **String** |  | 
**refundCauseId** | **Number** |  | [optional] 
**refundCauseDescription** | **String** |  | [optional] 
**name** | **String** | Name of item sold | [optional] 
**barcode** | **String** |  | [optional] 
**sku** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**notDiscountable** | **Boolean** |  | [optional] 
**price** | **Number** |  | 
**cost** | **Number** |  | [optional] 
**vatPerc** | **Number** |  | 
**finalPrice** | **Number** | Unit Price  + discount/surcharges | 
**finalNetPrice** | **Number** | Final Price without VAT | 
**quantity** | **Number** |  | 
**lastupdateAt** | **Date** |  | [optional] 
**lastupdateBy** | **Number** |  | [optional] 
**addedAt** | **Date** |  | [optional] 
**sellerId** | **Number** |  | 
**sellerName** | **String** |  | 
**categoryId** | **Number** |  | [optional] 
**categoryName** | **String** |  | [optional] 
**departmentId** | **Number** |  | [optional] 
**departmentName** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**priceChanges** | [**[PricechangeSchema]**](PricechangeSchema.md) |  | [optional] 
