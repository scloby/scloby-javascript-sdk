# SclobyApi.InlineResponse201

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**imageUrl** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**enabled** | **Boolean** |  | [optional] 
**allowAutomaticPrint** | **Boolean** |  | [optional] 
**forcePaid** | **Boolean** |  | [optional] 
**addToMailReceipts** | **Boolean** |  | [optional] 
**addToNewItems** | **Boolean** |  | [optional] 
**defaultPaymentMethodId** | **Number** |  | [optional] 
