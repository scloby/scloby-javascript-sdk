# SclobyApi.LoyaltyProgramPrizesApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**prizesGet**](LoyaltyProgramPrizesApi.md#prizesGet) | **GET** /prizes | Get All Prizes
[**prizesIdDelete**](LoyaltyProgramPrizesApi.md#prizesIdDelete) | **DELETE** /prizes/{id} | Delete existing Prize
[**prizesIdGet**](LoyaltyProgramPrizesApi.md#prizesIdGet) | **GET** /prizes/{id} | Get existing Prize
[**prizesIdPut**](LoyaltyProgramPrizesApi.md#prizesIdPut) | **PUT** /prizes/{id} | Edit existing Prize
[**prizesPost**](LoyaltyProgramPrizesApi.md#prizesPost) | **POST** /prizes | Add new Prize

<a name="prizesGet"></a>
# **prizesGet**
> ChainsRules prizesGet(opts)

Get All Prizes

Returns a Json with data about all prizes of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPrizesApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.prizesGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**ChainsRules**](ChainsRules.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="prizesIdDelete"></a>
# **prizesIdDelete**
> prizesIdDelete(id)

Delete existing Prize

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPrizesApi();
let id = "id_example"; // String | id of the Prize that need to be deleted

apiInstance.prizesIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Prize that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="prizesIdGet"></a>
# **prizesIdGet**
> ChainsPrizes prizesIdGet(id)

Get existing Prize

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPrizesApi();
let id = "id_example"; // String | id of the Prize

apiInstance.prizesIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Prize | 

### Return type

[**ChainsPrizes**](ChainsPrizes.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="prizesIdPut"></a>
# **prizesIdPut**
> ChainsPrizes prizesIdPut(bodyid)

Edit existing Prize

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPrizesApi();
let body = new SclobyApi.ChainsPrizes(); // ChainsPrizes | Object data that need to be updated
let id = "id_example"; // String | id of the Prize that need to be updated

apiInstance.prizesIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsPrizes**](ChainsPrizes.md)| Object data that need to be updated | 
 **id** | **String**| id of the Prize that need to be updated | 

### Return type

[**ChainsPrizes**](ChainsPrizes.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="prizesPost"></a>
# **prizesPost**
> InlineResponse2012 prizesPost(body)

Add new Prize

Returns a Json with the data of the new Prize

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPrizesApi();
let body = new SclobyApi.ChainsPrizes(); // ChainsPrizes | Prize object that needs to be added.

apiInstance.prizesPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsPrizes**](ChainsPrizes.md)| Prize object that needs to be added. | 

### Return type

[**InlineResponse2012**](InlineResponse2012.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

