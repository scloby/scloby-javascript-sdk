# SclobyApi.InlineResponse400Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**code** | **Number** |  | [optional] 
