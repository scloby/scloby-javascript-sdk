# SclobyApi.PricechangeSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**index** | **Number** |  | 
**type** | **String** | Type of price_change, can be discount_fix, discount_perc, surcharge_fix, surcharge_perc | 
**value** | **Number** | Value of discount/surcharge, for percentage values use 10.50 for 10.5% | 
**description** | **String** | Discount/surcharge description | 
**saleId** | **Number** |  | [optional] 
**saleItemId** | **Number** |  | [optional] 
**ruleId** | **Number** |  | [optional] 
**ruleName** | **String** |  | [optional] 
**prizeId** | **Number** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
