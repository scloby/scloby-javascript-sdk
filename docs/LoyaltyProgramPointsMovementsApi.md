# SclobyApi.LoyaltyProgramPointsMovementsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fidelitiesMovementsGet**](LoyaltyProgramPointsMovementsApi.md#fidelitiesMovementsGet) | **GET** /fidelities_movements | Get All Fidelity Movements
[**fidelitiesMovementsIdGet**](LoyaltyProgramPointsMovementsApi.md#fidelitiesMovementsIdGet) | **GET** /fidelities_movements/{id} | Get existing Fidelity Movement
[**fidelitiesMovementsPost**](LoyaltyProgramPointsMovementsApi.md#fidelitiesMovementsPost) | **POST** /fidelities_movements | Add new Fidelity Movement

<a name="fidelitiesMovementsGet"></a>
# **fidelitiesMovementsGet**
> ChainsFidelitiesMovements fidelitiesMovementsGet(opts)

Get All Fidelity Movements

Returns a Json with data about all fidelity movements of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPointsMovementsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.fidelitiesMovementsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="fidelitiesMovementsIdGet"></a>
# **fidelitiesMovementsIdGet**
> ChainsFidelitiesMovements fidelitiesMovementsIdGet(id)

Get existing Fidelity Movement

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPointsMovementsApi();
let id = "id_example"; // String | id of the Fidelity Movement

apiInstance.fidelitiesMovementsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Fidelity Movement | 

### Return type

[**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="fidelitiesMovementsPost"></a>
# **fidelitiesMovementsPost**
> ChainsFidelitiesMovements fidelitiesMovementsPost(body)

Add new Fidelity Movement

Returns a Json with the data of the new Fidelity Movement

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.LoyaltyProgramPointsMovementsApi();
let body = new SclobyApi.ChainsFidelitiesMovements(); // ChainsFidelitiesMovements | Fidelity Movement object that needs to be added.

apiInstance.fidelitiesMovementsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)| Fidelity Movement object that needs to be added. | 

### Return type

[**ChainsFidelitiesMovements**](ChainsFidelitiesMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

