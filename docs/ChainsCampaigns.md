# SclobyApi.ChainsCampaigns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**fromDate** | **Date** |  | [optional] 
**toDate** | **Date** |  | [optional] 
**active** | **Boolean** |  | 
**rulesApplicationOnPrizes** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
**isValid** | **Object** |  | [optional] 
**items** | [**[CampaignitemSchema]**](CampaignitemSchema.md) |  | [optional] 
**rules** | [**[RuleSchema]**](RuleSchema.md) |  | [optional] 
