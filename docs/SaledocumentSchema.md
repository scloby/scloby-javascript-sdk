# SclobyApi.SaledocumentSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**saleId** | **Number** |  | [optional] 
**sequentialNumber** | **Number** |  | [optional] 
**printerSerial** | **String** |  | [optional] 
**printerId** | **Number** |  | [optional] 
**printerName** | **String** |  | [optional] 
**_date** | **Date** |  | [optional] 
**documentType** | **String** |  | 
**documentUrl** | **String** |  | [optional] 
**documentContent** | **String** |  | [optional] 
**metadata** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**meta** | **Object** |  | [optional] 
