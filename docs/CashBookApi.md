# SclobyApi.CashBookApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cashMovementsGet**](CashBookApi.md#cashMovementsGet) | **GET** /cash_movements | Get All Cash Movements
[**cashMovementsIdDelete**](CashBookApi.md#cashMovementsIdDelete) | **DELETE** /cash_movements/{id} | Delete existing Cash Movement
[**cashMovementsIdGet**](CashBookApi.md#cashMovementsIdGet) | **GET** /cash_movements/{id} | Get existing Cash Movement
[**cashMovementsIdPut**](CashBookApi.md#cashMovementsIdPut) | **PUT** /cash_movements/{id} | Edit existing Cash Movement
[**cashMovementsPost**](CashBookApi.md#cashMovementsPost) | **POST** /cash_movements | Add new Cash movement

<a name="cashMovementsGet"></a>
# **cashMovementsGet**
> CashMovements cashMovementsGet(opts)

Get All Cash Movements

Returns a Json with data about all cash movements of selected shop.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CashBookApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.cashMovementsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="cashMovementsIdDelete"></a>
# **cashMovementsIdDelete**
> cashMovementsIdDelete(id)

Delete existing Cash Movement

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CashBookApi();
let id = "id_example"; // String | id of the Cash Movement that need to be deleted

apiInstance.cashMovementsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Cash Movement that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cashMovementsIdGet"></a>
# **cashMovementsIdGet**
> CashMovements cashMovementsIdGet(id)

Get existing Cash Movement

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CashBookApi();
let id = "id_example"; // String | id of the Cash Movement

apiInstance.cashMovementsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Cash Movement | 

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="cashMovementsIdPut"></a>
# **cashMovementsIdPut**
> CashMovements cashMovementsIdPut(bodyid)

Edit existing Cash Movement

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CashBookApi();
let body = new SclobyApi.CashMovements(); // CashMovements | Object data that need to be updated
let id = "id_example"; // String | id of the Cash Movement that need to be updated

apiInstance.cashMovementsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CashMovements**](CashMovements.md)| Object data that need to be updated | 
 **id** | **String**| id of the Cash Movement that need to be updated | 

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="cashMovementsPost"></a>
# **cashMovementsPost**
> CashMovements cashMovementsPost(body)

Add new Cash movement

Returns a Json with the data of the new Cash movement

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CashBookApi();
let body = new SclobyApi.CashMovements(); // CashMovements | Cash movement object that needs to be added.

apiInstance.cashMovementsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CashMovements**](CashMovements.md)| Cash movement object that needs to be added. | 

### Return type

[**CashMovements**](CashMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

