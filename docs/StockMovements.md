# SclobyApi.StockMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**itemId** | **Number** |  | [optional] 
**combinationId** | **Number** |  | [optional] 
**rawMaterialId** | **Number** |  | [optional] 
**entryType** | **String** |  | 
**name** | **String** |  | [optional] 
**barcode** | **String** |  | [optional] 
**code** | **String** |  | [optional] 
**optionsValues** | **String** |  | [optional] 
**combination** | **String** |  | [optional] 
**supplierId** | **Number** |  | [optional] 
**supplierName** | **String** |  | [optional] 
**supplierOrder** | **String** |  | [optional] 
**saleId** | **Number** |  | [optional] 
**quantity** | **Number** |  | 
**unit** | **String** |  | [optional] 
**type** | **String** |  | 
**loadCause** | **String** |  | [optional] 
**unloadCause** | **String** |  | [optional] 
**operatorId** | **Number** |  | [optional] 
**operatorUsername** | **String** |  | [optional] 
**operatorFullname** | **String** |  | [optional] 
**_date** | **Date** |  | 
**notes** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**updatedbyId** | **Number** |  | [optional] 
**deletedbyId** | **Number** |  | [optional] 
**operatorName** | **Object** |  | [optional] 
