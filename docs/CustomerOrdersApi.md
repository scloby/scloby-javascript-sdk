# SclobyApi.CustomerOrdersApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordersGet**](CustomerOrdersApi.md#ordersGet) | **GET** /orders | Get All Orders
[**ordersIdDelete**](CustomerOrdersApi.md#ordersIdDelete) | **DELETE** /orders/{id} | Delete existing Order
[**ordersIdGet**](CustomerOrdersApi.md#ordersIdGet) | **GET** /orders/{id} | Get existing Order
[**ordersIdPut**](CustomerOrdersApi.md#ordersIdPut) | **PUT** /orders/{id} | Edit existing Order
[**ordersPost**](CustomerOrdersApi.md#ordersPost) | **POST** /orders | Add new Order

<a name="ordersGet"></a>
# **ordersGet**
> Orders ordersGet(opts)

Get All Orders

Returns a Json with all orders of selected shop.  Paginated by default (per_page&#x3D;1000).    **Orders status**  An order can be in open or closed status. If is open it can be edited. Each order can contain a &#x27;previus_order&#x27; that is the previous version of the order, before last save (parcheggia) or print.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CustomerOrdersApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.ordersGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="ordersIdDelete"></a>
# **ordersIdDelete**
> ordersIdDelete(id)

Delete existing Order

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CustomerOrdersApi();
let id = "id_example"; // String | id of the Order that need to be deleted

apiInstance.ordersIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Order that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="ordersIdGet"></a>
# **ordersIdGet**
> Orders ordersIdGet(id)

Get existing Order

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CustomerOrdersApi();
let id = "id_example"; // String | id of the order

apiInstance.ordersIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the order | 

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="ordersIdPut"></a>
# **ordersIdPut**
> Orders ordersIdPut(bodyid)

Edit existing Order

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CustomerOrdersApi();
let body = new SclobyApi.Orders(); // Orders | Object data that need to be updated
let id = "id_example"; // String | id of the Order that need to be updated

apiInstance.ordersIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Orders**](Orders.md)| Object data that need to be updated | 
 **id** | **String**| id of the Order that need to be updated | 

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="ordersPost"></a>
# **ordersPost**
> Orders ordersPost(body)

Add new Order

Returns a Json with the data of the new Order

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.CustomerOrdersApi();
let body = new SclobyApi.Orders(); // Orders | Order object that needs to be added.

apiInstance.ordersPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Orders**](Orders.md)| Order object that needs to be added. | 

### Return type

[**Orders**](Orders.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

