# SclobyApi.Stock

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**itemId** | **Number** |  | [optional] 
**combinationId** | **Number** |  | [optional] 
**rawMaterialId** | **Number** |  | [optional] 
**itemOnSale** | **Boolean** |  | [optional] 
**stockQuantity** | **Number** |  | [optional] 
**available** | **String** |  | [optional] 
**unit** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**barcode** | **String** |  | [optional] 
**code** | **String** |  | [optional] 
**optionsValues** | **String** |  | [optional] 
**combination** | **String** |  | [optional] 
**lastupdateAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
