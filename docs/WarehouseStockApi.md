# SclobyApi.WarehouseStockApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stockIdGet**](WarehouseStockApi.md#stockIdGet) | **GET** /stock/{id} | Get existing Stock
[**stockSummaryGet**](WarehouseStockApi.md#stockSummaryGet) | **GET** /stock_summary | GET All stock summaries

<a name="stockIdGet"></a>
# **stockIdGet**
> Stock stockIdGet(id)

Get existing Stock

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.WarehouseStockApi();
let id = "id_example"; // String | id of the stock

apiInstance.stockIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the stock | 

### Return type

[**Stock**](Stock.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stockSummaryGet"></a>
# **stockSummaryGet**
> Stock stockSummaryGet(opts)

GET All stock summaries

Returns a Json with data about all stock summaries.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.WarehouseStockApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.stockSummaryGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**Stock**](Stock.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

