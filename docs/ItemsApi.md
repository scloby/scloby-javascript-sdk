# SclobyApi.ItemsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemsGet**](ItemsApi.md#itemsGet) | **GET** /items | GET All items
[**itemsIdDelete**](ItemsApi.md#itemsIdDelete) | **DELETE** /items/{id} | Delete existing Item
[**itemsIdGet**](ItemsApi.md#itemsIdGet) | **GET** /items/{id} | Get existing item
[**itemsIdPut**](ItemsApi.md#itemsIdPut) | **PUT** /items/{id} | Edit existing item
[**itemsPost**](ItemsApi.md#itemsPost) | **POST** /items | Add new Item

<a name="itemsGet"></a>
# **itemsGet**
> Items itemsGet(opts)

GET All items

Returns a Json with data about all items of selected shop.  Paginated by default (per_page&#x3D;1000)

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ItemsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination param
  'perPage': 56, // Number | results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.itemsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination param | [optional] 
 **perPage** | **Number**| results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="itemsIdDelete"></a>
# **itemsIdDelete**
> itemsIdDelete(id)

Delete existing Item

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ItemsApi();
let id = "id_example"; // String | id of the item that need to be deleted

apiInstance.itemsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the item that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="itemsIdGet"></a>
# **itemsIdGet**
> Items itemsIdGet(id)

Get existing item

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ItemsApi();
let id = "id_example"; // String | id of the item

apiInstance.itemsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the item | 

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="itemsIdPut"></a>
# **itemsIdPut**
> Items itemsIdPut(bodyid)

Edit existing item

In this case you must specify the id in the URL and change the data you wanna update.  As every scloby resource, you can do a PUT request. First of all you have to save the original content of the resource (simply doing a GET request), edit what do you want, and resend it in a PUT request.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ItemsApi();
let body = new SclobyApi.Items(); // Items | Object data that need to be updated
let id = "id_example"; // String | id of the item that need to be updated

apiInstance.itemsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Items**](Items.md)| Object data that need to be updated | 
 **id** | **String**| id of the item that need to be updated | 

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="itemsPost"></a>
# **itemsPost**
> Items itemsPost(body)

Add new Item

You must know first the **department id** and the **category id**. The category id can be not set, in this case its value will be null, and the item will be shown in the section No category of the app. If the item is a restaurant item you must know if he has **allergens** and insert the ids in the **allergents** node of json. If this item has **ingredients**, you can specify the **components** ids.

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.ItemsApi();
let body = new SclobyApi.Items(); // Items | Item object that needs to be added.

apiInstance.itemsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Items**](Items.md)| Item object that needs to be added. | 

### Return type

[**Items**](Items.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

