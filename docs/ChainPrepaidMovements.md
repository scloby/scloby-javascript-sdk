# SclobyApi.ChainPrepaidMovements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**customerUuid** | **String** |  | 
**previousId** | **Number** |  | [optional] 
**shopUuid** | **String** |  | [optional] 
**validFrom** | **Date** |  | 
**validTo** | **Date** |  | [optional] 
**credit** | **Number** |  | 
**ticketCredit** | **Number** |  | 
**movementTypeId** | **Number** |  | 
**saleUuid** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**createdbyId** | **Number** |  | [optional] 
**amount** | **Object** |  | [optional] 
**ticketAmount** | **Object** |  | [optional] 
**movementType** | **Object** |  | [optional] 
