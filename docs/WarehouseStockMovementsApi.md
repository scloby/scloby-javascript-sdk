# SclobyApi.WarehouseStockMovementsApi

All URIs are relative to *https://api.scloby.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stockMovementsGet**](WarehouseStockMovementsApi.md#stockMovementsGet) | **GET** /stock_movements | Get All Stock Movements
[**stockMovementsIdDelete**](WarehouseStockMovementsApi.md#stockMovementsIdDelete) | **DELETE** /stock_movements/{id} | Delete existing Stock Movement
[**stockMovementsIdGet**](WarehouseStockMovementsApi.md#stockMovementsIdGet) | **GET** /stock_movements/{id} | Get existing stock movement
[**stockMovementsIdPut**](WarehouseStockMovementsApi.md#stockMovementsIdPut) | **PUT** /stock_movements/{id} | Edit existing Stock Movement
[**stockMovementsPost**](WarehouseStockMovementsApi.md#stockMovementsPost) | **POST** /stock_movements | Add new Stock Movements

<a name="stockMovementsGet"></a>
# **stockMovementsGet**
> StockMovements stockMovementsGet(opts)

Get All Stock Movements

Returns a Json with data about all stock movements of selected shop.  Paginated by default (default per_page &#x3D; 1000)

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.WarehouseStockMovementsApi();
let opts = { 
  'pagination': true, // Boolean | Pagination parameter
  'perPage': 56, // Number | Results_per_page
  'page': 56 // Number | PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1)
};
apiInstance.stockMovementsGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pagination** | **Boolean**| Pagination parameter | [optional] 
 **perPage** | **Number**| Results_per_page | [optional] 
 **page** | **Number**| PAGE_NUMBER(starting from 0 to TOTAL_PAGE-1) | [optional] 

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stockMovementsIdDelete"></a>
# **stockMovementsIdDelete**
> stockMovementsIdDelete(id)

Delete existing Stock Movement

In this case you must specify the id in the URL, but it is no necessary in the request body

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.WarehouseStockMovementsApi();
let id = "id_example"; // String | id of the Stock Movement that need to be deleted

apiInstance.stockMovementsIdDelete(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the Stock Movement that need to be deleted | 

### Return type

null (empty response body)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="stockMovementsIdGet"></a>
# **stockMovementsIdGet**
> StockMovements stockMovementsIdGet(id)

Get existing stock movement

In this case you must specify the id in the URL

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.WarehouseStockMovementsApi();
let id = "id_example"; // String | id of the stock movement

apiInstance.stockMovementsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id of the stock movement | 

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="stockMovementsIdPut"></a>
# **stockMovementsIdPut**
> StockMovements stockMovementsIdPut(bodyid)

Edit existing Stock Movement

In this case you must specify the id in the URL and change the data you wanna update

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.WarehouseStockMovementsApi();
let body = new SclobyApi.StockMovements(); // StockMovements | Object data that need to be updated
let id = "id_example"; // String | id of the stock movement that need to be updated

apiInstance.stockMovementsIdPut(bodyid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StockMovements**](StockMovements.md)| Object data that need to be updated | 
 **id** | **String**| id of the stock movement that need to be updated | 

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="stockMovementsPost"></a>
# **stockMovementsPost**
> StockMovements stockMovementsPost(body)

Add new Stock Movements

Returns a Json with the data of the new Stock Movements.  **Stock movements type**  Each Stock Movement can be one of these three types: - load (increase quantity) - unload (decrease quantity) - setup (set quantity)  and can be referred to: - Item (item_id is not null and raw_material is null) - Raw Material (raw_material_id is not null and item_id is null)   **Stock movements causes**  Load causes: - buy - refund - gift - replacement - production   Unload causes: - sale - gift - move - damage - theft - replacement - waste          

### Example
```javascript
import SclobyApi from 'scloby_api';
let defaultClient = SclobyApi.ApiClient.instance;

// Configure OAuth2 access token for authorization: oAuth2AuthCode
let oAuth2AuthCode = defaultClient.authentications['oAuth2AuthCode'];
oAuth2AuthCode.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new SclobyApi.WarehouseStockMovementsApi();
let body = new SclobyApi.StockMovements(); // StockMovements | Item object that needs to be added.

apiInstance.stockMovementsPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StockMovements**](StockMovements.md)| Item object that needs to be added. | 

### Return type

[**StockMovements**](StockMovements.md)

### Authorization

[oAuth2AuthCode](../README.md#oAuth2AuthCode)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

